# git-calver

git-calver is a shell script, which automates the creation of software release 
numbers in the form of YYYY.MONTH.REVISION using git tags.

For more information read the online documentation git-calver(1).

Install by running

$ make install